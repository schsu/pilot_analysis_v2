#include<TString.h>
#include<TFile.h>
#include<TNtuple.h>
#include<TLeaf.h>
#include<stdlib.h>
#include<vector>
#include<iostream>
#include<iterator>
#include<map>
void subtract(TString filename,TString linkfilename,TString outfilename){
	TFile f(filename),g(linkfilename),h(outfilename,"RECREATE");
	TTree *t[29],*tlink[29],*ts[29];
	float x,y,z,tx,ty,etruth;
	int id,iz,pdgid,clstruth,idlink;
	for(int lyr=0;lyr<29;lyr++){
		iz = lyr+90;
		t[lyr] = (TTree*)f.Get(Form("tree%d",lyr+90));
		tlink[lyr] = (TTree*)g.Get(Form("tree%d",lyr+90));
		ts[lyr] = new TTree(Form("tree%d",lyr+90),Form("BTs in layer %d",lyr+90));
		int nentries = t[lyr]->GetEntries();
		int nentrieslink = tlink[lyr]->GetEntries();
		if(nentries <= nentrieslink ){
			cout << "Layer "<< lyr <<" Input BTs "<< nentries <<" isn't greater than Link BTs "<< nentrieslink << endl;
			continue;
		}
		t[lyr]->SetBranchAddress("id",&id);
		t[lyr]->SetBranchAddress("x",&x);
		t[lyr]->SetBranchAddress("y",&y);
		t[lyr]->SetBranchAddress("z",&z);
		t[lyr]->SetBranchAddress("tx",&tx);
		t[lyr]->SetBranchAddress("ty",&ty);
		t[lyr]->SetBranchAddress("pdgid",&pdgid);
		t[lyr]->SetBranchAddress("clstruth",&clstruth);
		t[lyr]->SetBranchAddress("etruth",&etruth);
		tlink[lyr]->SetBranchAddress("id",&idlink);
		ts[lyr]->Branch("x",&x,"x/F");
		ts[lyr]->Branch("y",&y,"y/F");
		ts[lyr]->Branch("z",&z,"z/F");
		ts[lyr]->Branch("iz",&iz,"iz/I");
		ts[lyr]->Branch("id",&id,"id/I");
		ts[lyr]->Branch("tx",&tx,"tx/F");
		ts[lyr]->Branch("ty",&ty,"ty/F");
		ts[lyr]->Branch("pdgid",&pdgid,"pdgid/I");
		ts[lyr]->Branch("clstruth",&clstruth,"clstruth/I");
		ts[lyr]->Branch("etruth",&etruth,"etruth/F");
		cout << nentries << " " << nentrieslink << endl;
		map<int,vector<float>> btmap;
		for(int i=0;i<nentries;i++){
			t[lyr]->GetEntry(i);
			btmap.insert(pair<int,vector<float>>{id,{x,y,z,tx,ty,static_cast<float>(pdgid),static_cast<float>(clstruth),etruth}});
		}
		for(int i=0;i<nentrieslink;i++){
			tlink[lyr]->GetEntry(i);
			btmap.erase(idlink);
		}
		int ct = 0;
		map<int, vector<float>>::iterator itr;
		for (itr = btmap.begin(); itr != btmap.end(); ++itr) {
			ct++;
			id = itr->first;
			x = itr->second[0];
			y = itr->second[1];
			z = itr->second[2];
			tx = itr->second[3];
			ty = itr->second[4];
			pdgid = itr->second[5];
			clstruth = itr->second[6];
			etruth = itr->second[7];
			ts[lyr]->Fill();
		}
		cout << nentries << "-" << nentrieslink << "=" << ct << endl;

	}
	for(int lyr=0;lyr<29;lyr++)ts[lyr]->Write();
	h.Close();
	g.Close();
	f.Close();
}
