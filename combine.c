#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TLeaf.h>
#include<TString.h>
#include<iostream>
#include<stdlib.h>

using namespace std;
void combine(TString outputfilename,double xmin,double xmax,double ymin,double ymax){
	TFile fout(outputfilename,"RECREATE");
	Float_t x,y,z,tx,ty,etruth;
	Int_t pdgid,clstruth;
	TTree *tw[29];
	for(int ipl=90;ipl<119;ipl++){
		tw[ipl-90] = new TTree(Form("tree%d",ipl),Form("BTs in layer %d",ipl));
		tw[ipl-90]->Branch("x",&x,"x/F");
		tw[ipl-90]->Branch("y",&y,"y/F");
		tw[ipl-90]->Branch("z",&z,"z/F");
		tw[ipl-90]->Branch("tx",&tx,"tx/F");
		tw[ipl-90]->Branch("ty",&ty,"ty/F");
		tw[ipl-90]->Branch("pdgid",&pdgid,"pdgid/I");
		tw[ipl-90]->Branch("clstruth",&clstruth,"clstruth/I");
		tw[ipl-90]->Branch("etruth",&etruth,"etruth/F");
	}
	for(int i=1;i<11;i++){
		TFile f(Form("/eos/project-f/fasernu-pilot-run/work/js_pilot_mc/muminus%d.root",i));
		for(int ipl=90;ipl<119;ipl++){
			printf("Mu- file %d plate %d\n",i,ipl);
			TTree *tr = (TTree*)f.Get(Form("tree%d",ipl));
			tr->SetBranchAddress("x",&x);
			tr->SetBranchAddress("y",&y);
			tr->SetBranchAddress("z",&z);
			tr->SetBranchAddress("tx",&tx);
			tr->SetBranchAddress("ty",&ty);
			tr->SetBranchAddress("pdgid",&pdgid);
			tr->SetBranchAddress("clstruth",&clstruth);
			tr->SetBranchAddress("etruth",&etruth);
			int nentries = tr->GetEntries();
			printf("%d %d\n",ipl,nentries);
			for(int j=0;j<nentries;j++){
				tr->GetEntry(j);
				if(x<xmin||x>xmax)continue;
				if(y<ymin||y>ymax)continue;
				clstruth+=(i-1)*1100000;
				x*=1000;
				y*=1000;
				z*=1000;
				if(j%100000==0)printf("%d / %d %f %f %f %f %f %d %d %f\n",j,nentries,x,y,z,tx,ty,pdgid,clstruth,etruth);
				tw[ipl-90]->Fill();
			}
		}
	f.Close();
	}
	for(int i=1;i<11;i++){
		TFile f(Form("/eos/project-f/fasernu-pilot-run/work/js_pilot_mc/muplus%d.root",i));
		for(int ipl=90;ipl<119;ipl++){
			printf("Mu+ file %d plate %d\n",i,ipl);
			TTree *tr = (TTree*)f.Get(Form("tree%d",ipl));
			tr->SetBranchAddress("x",&x);
			tr->SetBranchAddress("y",&y);
			tr->SetBranchAddress("z",&z);
			tr->SetBranchAddress("tx",&tx);
			tr->SetBranchAddress("ty",&ty);
			tr->SetBranchAddress("pdgid",&pdgid);
			tr->SetBranchAddress("clstruth",&clstruth);
			tr->SetBranchAddress("etruth",&etruth);
			int nentries = tr->GetEntries();
			printf("%d %d\n",ipl,nentries);
			for(int j=0;j<nentries;j++){
				tr->GetEntry(j);
				if(x<xmin||x>xmax)continue;
				if(y<ymin||y>ymax)continue;
				clstruth+=(i+20)*1100000;
				x*=1000;
				y*=1000;
				z*=1000;
				if(j%100000==0)printf("%d / %d %f %f %f %f %f %d %d %f\n",j,nentries,x,y,z,tx,ty,pdgid,clstruth,etruth);
				tw[ipl-90]->Fill();
			}
		}
	f.Close();
	}

	fout.cd();
	for(int ipl=90;ipl<119;ipl++)tw[ipl-90]->Write();
	printf("SUCCESS\n");
}
