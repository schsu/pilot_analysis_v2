# pilot_analysis_v2

Instructions:

# 0. Clone repo
git clone https://gitlab.cern.ch/jwspence/pilot_analysis_v2.git

# 1. Build FEDRA

source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/5.34.36/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh

cd pilot_analysis_v2/Fedra

./install.sh

# 2. Compile C++ executables
cd ..

./compile.sh

# 3. Combine sim files:
root -l -q 'combine.c+("area00.root",-62.5,-42.5,-50,-30)'

# 4. Make unique BTIDs:
root -l -q 'makeid.c+("area00.root","id00.root")'

# 5. Run ghost rejection (necessary for tracking):
./jsghostrejection id00.root gr00.root

# 6. Run tracking:
./jstracking gr00.root lt00.root

# 7. Preprocess tracking file:
root -l -q 'process.c+("lt00.root","pr00.root")'

# 8. Subtract muon BTs from id:

ROOT6 is required for the subtract macro

source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.14.04/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh

This macro generates input for electron seeding/clustering. It subtracts track BTs from all BTs

root -l -q 'subtract.c+("id00.root","pr00.root","s00.root")'

# 9. Electron seeding

# 10. Electron clustering

# 11. Electron energy regression

# MC/Data comparison

## BT distribuion.

Jupyter notebook "Absolute Comparison.ipynb"  compares BT distributions in both files.

Download MC and Data files to local disk, e.g. 
area00.root is MC sample with the area of 20 mm x 20 mm. It can be directly compared to 
/eos/project-f/fasernu-pilot-run/Data/TS2/TS2Pb/ONLINE/b100012/dset_TS_p090-118/dataarea12.root 


