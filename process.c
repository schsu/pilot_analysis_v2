#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TLeaf.h>
#include<TString.h>
#include<stdlib.h>
#include<iostream>

using namespace std;
void process(TString inputfilename,TString outputfilename){
	TFile f(inputfilename);
	TTree *t = (TTree*)f.Get("tracks");
	int nentries = t->GetEntries();
	TBranch *b = t->FindBranch("s");
	TLeaf *xl = t->FindLeaf("eX");
	TLeaf *yl = t->FindLeaf("eY");
	TLeaf *zl = t->FindLeaf("eZ");
	TLeaf *izl = t->FindLeaf("s.eScanID.ePlate");
	TLeaf *txl = t->FindLeaf("eTX");
	TLeaf *tyl = t->FindLeaf("eTY");
	TLeaf *idl = t->FindLeaf("eID");
	TLeaf *clstruthl = t->FindLeaf("eMCEvt");
	TLeaf *pdgidl = t->FindLeaf("eMCTrack");
	TLeaf *etruthl = t->FindLeaf("eP");
	cout << xl << " " << yl << " " << zl << " " << izl << " " << txl << " " << tyl << " " << idl << " " << clstruthl << " " << pdgidl << " " << etruthl << endl;
	float x,y,z,tx,ty,etruth;
	int iz,id,clstruth,pdgid;
	TFile g(outputfilename,"RECREATE");
	TTree *tw[29];
	for(int i=0;i<29;i++){
		tw[i] = new TTree(Form("tree%d",i+90),Form("BTs in layer %d",i+90));
		tw[i]->Branch("x",&x);
		tw[i]->Branch("y",&y);
		tw[i]->Branch("z",&z);
		tw[i]->Branch("iz",&iz);
		tw[i]->Branch("tx",&tx);
		tw[i]->Branch("ty",&ty);
		tw[i]->Branch("pdgid",&pdgid);
		tw[i]->Branch("clstruth",&clstruth);
		tw[i]->Branch("etruth",&etruth);
		tw[i]->Branch("id",&id);
	}
	for(int i=0;i<nentries;i++){
		t->GetEntry(i);
		int llen = xl->GetLen();
		for(int j=0;j<llen;j++){
			x = xl->GetValue(j);
			y = yl->GetValue(j);
			z = zl->GetValue(j);
			tx = txl->GetValue(j);
			ty = tyl->GetValue(j);
			iz = izl->GetValue(j);
			id = idl->GetValue(j);
			pdgid = pdgidl->GetValue(j);
			clstruth = clstruthl->GetValue(j);
			etruth = etruthl->GetValue(j);
			if(i%1000==0)cout << i << "/" << nentries << " " << j << "/" << llen << " " << x << " " << y << " " << z << " " << iz << " " << tx << " " << ty << " " << id << " " << pdgid << " " << clstruth << " " << etruth << endl;
			tw[iz-90]->Fill();
		}
	}
	for(int i=0;i<29;i++)tw[i]->Write();
	g.Close();
	f.Close();
}
